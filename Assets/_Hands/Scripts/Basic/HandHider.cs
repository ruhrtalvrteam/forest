﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class HandHider : MonoBehaviour
{
    public GameObject handObject = null;

    private HandPhysics handPhysics = null;
    private XRDirectInteractor interactor = null;
    private XRRayInteractor rayinteractor = null;

    private void Awake()
    {
        handPhysics = handObject.GetComponent<HandPhysics>();
        interactor = GetComponent<XRDirectInteractor>();
        rayinteractor = GetComponent<XRRayInteractor>();
    }

    private void OnEnable()
    {
        if (interactor != null)
        {
            interactor.onSelectEnter.AddListener(Hide);
            interactor.onSelectExit.AddListener(Show);
        }

        if (rayinteractor != null)
        {
            rayinteractor.onSelectEnter.AddListener(Hide);
            rayinteractor.onSelectExit.AddListener(Show);
        }
    }

    private void OnDisable()
    {
        if (interactor != null)
        {
            interactor.onSelectEnter.RemoveListener(Hide);
            interactor.onSelectExit.RemoveListener(Show);
        }

        if (rayinteractor != null)
        {
            rayinteractor.onSelectEnter.RemoveListener(Hide);
            rayinteractor.onSelectExit.RemoveListener(Show);
        }
    }

    private void Show(XRBaseInteractable interactable)
    {
        handPhysics.TeleportToTarget();
        handObject.SetActive(true);
    }

    private void Hide(XRBaseInteractable interactable)
    {
        handObject.SetActive(false);
    }
}
