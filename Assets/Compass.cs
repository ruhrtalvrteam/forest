﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class Compass : MonoBehaviour
{
    [SerializeField]
    private GameObject[] Targets;
    private XRGrabInteractable TargetGrab;

    // Start is called before the first frame update
    void Awake()
    {
        Targets = GameObject.FindGameObjectsWithTag("Gold");
    }

    public GameObject FindClosestTarget()
    {
        if (Targets.Length == 0)
        {
            return null;
        }
        else
        {
            GameObject closest = null;
            float distance = Mathf.Infinity;
            Vector3 position = transform.position;
            foreach (GameObject Target in Targets)
            {
                TargetGrab = Target.GetComponent<XRGrabInteractable>();

                if (!TargetGrab.isSelected)
                {
                    Vector3 diff = Target.transform.position - position;
                    float curDistance = diff.sqrMagnitude;
                    if (curDistance < distance)
                    {
                        closest = Target;
                        distance = curDistance;
                    }
                }
            }
            return closest;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (FindClosestTarget() != null)
        {
            gameObject.transform.LookAt(FindClosestTarget().transform);
        }


    }
}
