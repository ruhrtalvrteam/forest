﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharController : MonoBehaviour
{
    public Transform[] points;
    int destPoint = 0;
    NavMeshAgent agent;
    public Animator animator;

    bool hasGrabbed = false;
    bool hasGrabbedFinished = false;
    //bool hasKneed = false;

    public Transform AttachTransform;

    public float distanceTarget;
    public float distancePlayer;
    public float StoppingDistance;
    //public float distanceOffset = 1.0f;

    // Start is called before the first frame update
    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        //animator = GetComponentInChildren<Animator>();

        StoppingDistance = agent.stoppingDistance;

        //distancePlayer = Vector3.Distance(transform.position, target.position);
    }

    void Start()
    {
        // Choose the next destination point when the agent gets to the current one.
        GotoNextPoint();
    }

    void Update()
    {
        // Updates Player Position
        points[1].position = GameObject.Find("Player").transform.position;

        if (!hasGrabbed)
        {
            if (!agent.pathPending && agent.remainingDistance <= StoppingDistance)
            {
                hasGrabbed = true;
                StartCoroutine(GrabFunction());
            }
            
        }

        if (hasGrabbed && hasGrabbedFinished)
        {
            if (!agent.pathPending && agent.remainingDistance <= StoppingDistance)
            {
                StartCoroutine(KneeFunction());
            }
        }
    }

    IEnumerator GrabFunction()
    {
        /*while (animator.IsInTransition(0))
        {
            Debug.Log("Waiting to Grab");
            
        }
        */

        Debug.Log(animator.IsInTransition(0));

        Debug.Log("Grabbing");
        animator.SetTrigger("grab");
        
        //while(animator.IsInTransition(0) || animator.GetCurrentAnimatorStateInfo(0).IsName("Grab"))
        //    yield return new WaitForSeconds(0.2f);

        yield return new WaitForSeconds(4f);

        GotoNextPoint();

        yield return null;
    }

    IEnumerator KneeFunction()
    {
        yield return new WaitForSeconds(6f);

        Debug.Log("Kneeing");
        animator.SetTrigger("knee");

        yield return null;
    }

    void GotoNextPoint()
    {
        // Returns if no points have been set up
        if (points.Length == 0)
            return;

        // Set the agent to go to the currently selected destination.
        FaceTarget(points[destPoint]);
        agent.destination = points[destPoint].position;

        // Choose the next point in the array as the destination,
        // cycling to the start if necessary.
        destPoint = (destPoint + 1) % points.Length;

        hasGrabbedFinished = true;
    }

    void MoveToPosition(Transform trans)
    {
        FaceTarget(trans);
        agent.SetDestination(trans.position);
    }

    void FaceTarget(Transform trans)
    {
        Vector3 direction = (trans.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 15f);
    }
}
