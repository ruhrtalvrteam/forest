﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class ProximitySound : MonoBehaviour
{
    XRSocketInteractor Socket;

    private void Awake()
    {
        Socket = GetComponentInParent<XRSocketInteractor>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player") && Socket.selectTarget != null)
        {
            gameObject.GetComponent<AudioSource>().Play();
        }
    }
}
