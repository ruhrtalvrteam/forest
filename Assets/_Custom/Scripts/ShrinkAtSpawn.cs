﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShrinkAtSpawn : MonoBehaviour
{
    private float wait = 3.0f;

    public Transform[] ChildrenTransform;

    // Start is called before the first frame update
    void Start()
    {
        ChildrenTransform = gameObject.GetComponentsInChildren<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if(wait >= 0)
        {
            wait -= Time.deltaTime;
        }
        else
        {
            foreach (Transform ChildTransform in ChildrenTransform)
            {
                
                ChildTransform.localScale = Vector3.Lerp(ChildTransform.localScale, ChildTransform.localScale * 0, Time.deltaTime * 2);
            }
        }
    }
}
