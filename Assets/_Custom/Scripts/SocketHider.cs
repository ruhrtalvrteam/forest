﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class SocketHider : MonoBehaviour
{
    private XRSocketInteractor Socket;
    private MeshRenderer ItemRenderer;

    // Start is called before the first frame update
    void Start()
    {
        Socket = GetComponent<XRSocketInteractor>();
    }

    public void HideItem()
    {
        if (Socket.selectTarget != null)
        {
            ItemRenderer = Socket.selectTarget.gameObject.GetComponent<MeshRenderer>();
            ItemRenderer.enabled = false;
        }
    }

    public void ShowItem()
    {
        if(ItemRenderer != null)
        {
            ItemRenderer.enabled = true;
        }
        ItemRenderer = null;
    }
}
