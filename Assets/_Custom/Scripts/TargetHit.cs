﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetHit : MonoBehaviour, IDamageable
{
    public GameObject ParentObject = null;
    public GameObject ParticleObject = null;
    public GameObject ItemObject = null;
    public GameObject DestroyedVersion = null;

    [SerializeField]
    private ParticleSystem Particle = null;

    [SerializeField]
    private Rigidbody ItemRig = null;

    // Start is called before the first frame update
    void Start()
    {

    }


    public void Damage(int amount)
    {
        ReleaseItem();
        TriggerParticle();
        DestroyObject();
    }

    private void ReleaseItem()
    {
        if (ItemRig != null)
        {
            ItemRig.useGravity = true;
            ItemRig.isKinematic = false;
        }
    }

    private void TriggerParticle()
    {
        if(Particle != null)
            Particle.Play();
    }

    private void DestroyObject()
    {
        ParentObject.SetActive(false);
        Instantiate(DestroyedVersion, ParentObject.transform.position, ParentObject.transform.rotation);
    }
}
