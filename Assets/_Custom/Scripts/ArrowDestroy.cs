﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowDestroy : MonoBehaviour
{
    public void DestroyArrow()
    {
        Destroy(gameObject, 5.0f);
    }
}
