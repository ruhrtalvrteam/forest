﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MovementController : MonoBehaviour
{
    [SerializeField]
    private Transform Sphere = null;

    [SerializeField]
    private Transform Player = null;

    [SerializeField]
    private Transform Target1 = null;

    [SerializeField]
    private Transform Target2 = null;

    [SerializeField]
    private CanvasGroup canvas = null;

    private Animator anim = null;
    private NavMeshAgent agent = null;

    [SerializeField]
    private GameObject Ring = null;

    private bool destinationTarget1 = false;
    private bool destinationTarget2 = false;
    private bool hasGrabbed = false;
    private bool hasKneed = false;

    private bool finished = false;

    private float animationTime = 0;


    private AudioSource footsteps = null;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animator>();
        footsteps = GetComponent<AudioSource>();

        if (Player != null && Sphere != null)
        {
            Target1 = Sphere;
            Target2 = Player;
        }

        Ring.transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
        Ring.SetActive(false);
    }

    private void FixedUpdate()
    {
        if(Player != null && Sphere != null)
        {
            Target1 = Sphere;
            Target2 = Player;
        }

        if (agent.velocity.magnitude > 0.2 && GetComponent<AudioSource>().isPlaying == false)
        {
            footsteps.volume = Random.Range(0.8f, 1);
            footsteps.pitch = Random.Range(0.8f, 1.1f);
            footsteps.Play();
        }
    }

    private void Start()
    {
        agent.destination = Target1.position;
        destinationTarget1 = true;
        Sphere.gameObject.SetActive(false);
    }

    private void Update()
    {
        if(!finished)
        {
            // Check for Grab
            if(destinationTarget1 && !hasGrabbed && !hasKneed)
            {
                agent.destination = Target1.position;
                FaceTarget(Target1);

                float dist = Vector3.Distance(transform.position, Target1.position);

                if (dist <= (agent.stoppingDistance + 1f))
                {
                    if (!hasGrabbed)
                    {
                        //anim.SetTrigger("grab");
                        hasGrabbed = true;
                        //animationTime = 4.0f;
                        animationTime = 0f;
                    }
                }
            }
            else if (animationTime > 0 && hasGrabbed && !hasKneed)
            {
                animationTime -= Time.deltaTime;
            }

            // Send Agent to next Position
            if (destinationTarget1 && hasGrabbed && animationTime <= 0.1)
            {
                agent.destination = Target2.position;
                destinationTarget1 = false;
                destinationTarget2 = true;
            }

            // Check for Knee
            if (!destinationTarget1 && destinationTarget2 && hasGrabbed && !hasKneed)
            {
                agent.destination = Target2.position;
                FaceTarget(Target2);

                float dist = Vector3.Distance(transform.position, Target2.position);

                if (dist <= (agent.stoppingDistance + 0.6f))
                {
                    anim.SetTrigger("knee");
                    hasKneed = true;
                    finished = true;

                    StartCoroutine(CanvasAlpha());
                }
            }
        }

    }

    IEnumerator CanvasAlpha()
    {
        yield return new WaitForSeconds(6.0f);
        LeanTween.alphaCanvas(canvas, 1, 1);

        yield return null;
    }


    bool isMoving()
    {
        if (agent.velocity.magnitude > 0.1)
            return true;
        else
            return false;
    }



    void FaceTarget(Transform trans)
    {
        Vector3 direction = (trans.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 15f);
    }
}
