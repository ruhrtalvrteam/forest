﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingFollow : MonoBehaviour
{
    public Transform target;
    public Vector3 offset;

    void FixedUpdate()
    {
        transform.position = target.position;


        transform.eulerAngles = new Vector3(target.eulerAngles.x, target.eulerAngles.y, target.eulerAngles.z);
    }
}
