﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingHandler : MonoBehaviour
{

    public GameObject Ring;

    void ShowRing()
    {
        if (Ring != null)
        {
            Ring.SetActive(true);
            LeanTween.scale(Ring, new Vector3(0.8f, 0.8f, 0.8f), 0.5f);
        }

    }
}
