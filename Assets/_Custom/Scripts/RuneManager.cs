﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.UI;

public class RuneManager : MonoBehaviour
{
    private RuneCoroutines[] RuneAttributes;

    [SerializeField]
    private GameObject Sphere = null;

    private int RuneQuestFinished = 0;
    private bool _QuestFinished = false;

    [SerializeField]
    private Text ProgressText = null;

    //public bool _SpawnTriggerHit = false;


    private void Awake()
    {
        RuneAttributes = RuneCoroutines.FindObjectsOfType<RuneCoroutines>();
    }

    private void Start()
    {
        StartCoroutine(CoroutineSocketStatus());
    }

    IEnumerator CoroutineSocketStatus()
    {
        // Reset the counter each run
        RuneQuestFinished = 0;

        foreach (RuneCoroutines Rune in RuneAttributes)
        {
            XRSocketInteractor RuneSocket = Rune.GetComponent<XRSocketInteractor>();

            // Check if the runes have been socketed.
            //If any is missing, the script resets and count again later
            if (RuneSocket.selectTarget != null)
            {
                RuneQuestFinished++;

            }
            yield return new WaitForSeconds(0.2f);
        }

        ProgressText.text = RuneQuestFinished + "/" + RuneAttributes.Length;

        // Check if every Rune reports finished, sets the booleon true, to end the loop
        if (RuneQuestFinished == RuneAttributes.Length)
            _QuestFinished = true;

        // Check if Quest is finished
        if (!_QuestFinished)
        {
            // Delay everything afterwards
            yield return new WaitForSeconds(1.0f);

            //Setup another run of the coroutine
            StartCoroutine(CoroutineSocketStatus());
        }
        else
        {
            //Debug.Log("Quest finished!" + ", Runes finished " + RuneQuestFinished + "/" + RuneAttributes.Length);

            StartCoroutine(CoroutineActivateRune());
            yield return null;
        }
    }


    // Coroutine activating the Runes Animations and Spawn holograms
    IEnumerator CoroutineActivateRune()
    {
        // Delay for this Coroutine
        yield return new WaitForSeconds(0.0f);

        // Instantiate Activate FX
        foreach (RuneCoroutines Rune in RuneAttributes)
        {
            XRSocketInteractor RuneSocket = Rune.GetComponent<XRSocketInteractor>();

            // Disable item pull from socket
            //RuneSocket.socketActive = false;

            // FX
            Rune.fxSocketed.Stop();
            Rune.fxActivate.Play();
            yield return new WaitForSeconds(0.2f);
        }

        // Delay
        yield return new WaitForSeconds(0.4f);


        // Instantiate Hologram FX
        foreach (RuneCoroutines Rune in RuneAttributes)
        {
            Rune.fxActivate.Stop();
            yield return new WaitForSeconds(0.1f);
            Rune.fxHologram.Play();

            // Delay for Rippleeffect
            yield return new WaitForSeconds(0.2f);
        }

        // Instantiate Hologram Lerptransition
        foreach (RuneCoroutines Rune in RuneAttributes)
        {
            GameObject Hologram = Rune.AttachHologram.gameObject;
            Hologram.SetActive(true);

            Rune.StartHologramLerp();

            // Delay for Rippleeffect
            yield return new WaitForSeconds(0.5f);
        }

        // Delay
        yield return new WaitForSeconds(0.4f);


        // Delay
        yield return new WaitForSeconds(10.0f);

        // Fuse gold pieces
        foreach (RuneCoroutines Rune in RuneAttributes)
        {
            Rune.StartGoldLerp(gameObject.transform);

            yield return new WaitForSeconds(0.0f);
        }

        yield return new WaitForSeconds(2.5f);

        // Activate Interactable of the RuneManager
        XRSimpleInteractable iComponent = gameObject.transform.GetComponent<XRSimpleInteractable>();
        iComponent.enabled = true;

        Sphere.SetActive(true);

        //Trigger Animation for Gold towards target


        // Scale holograms towards zero after trigger has been switched (glowing orb in the center)


        //
        yield return null;
    }

    IEnumerator PlayFX(ParticleSystem ParticleFX)
    {
        // Check if FX is already Playing
        while (ParticleFX.IsAlive())
            yield return new WaitForSeconds(0.1f);

        ParticleFX.Play();

        yield return null;
    }

    IEnumerator StopFX(ParticleSystem ParticleFX)
    {
        if (ParticleFX.IsAlive())
            ParticleFX.Stop();

        yield return null;
    }
}
