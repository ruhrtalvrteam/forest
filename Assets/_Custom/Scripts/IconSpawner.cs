﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class IconSpawner : MonoBehaviour
{
    private GameObject Player;
    public GameObject IconPrefab;
    private GameObject FloatingIcon;
    private MeshRenderer Renderer;

    private XRBaseInteractable Interactable;


    public bool _IsShown;
    public bool _IsPickedup;
    private void Awake()
    {
        Player = GameObject.FindGameObjectWithTag("MainCamera");


        FloatingIcon = Instantiate(IconPrefab, gameObject.transform.position, Quaternion.identity);
        FloatingIcon.transform.position = transform.position;
        LeanTween.alpha(FloatingIcon, 0f, 0f);

        Renderer = FloatingIcon.GetComponent<MeshRenderer>();

        Interactable = GetComponent<XRGrabInteractable>();
    }

    

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Interactable.isSelected)
            Renderer.enabled = false;
        else
            Renderer.enabled = true;


        if (ProximityCheck(3f, Player))
        {
            if (!_IsShown)
            {
                ShowIcon(1.5f);
            }
        }
        else
        {
            if (_IsShown)
            {
                HideIcon(1.5f);
            }
        }

        LookAtPlayer(FloatingIcon, Player);

        // Follow Object
        FloatingIcon.transform.position = transform.position;


    }

    bool ProximityCheck(float triggerDistance, GameObject Target)
    {
        float dist = Vector3.Distance(transform.position, Target.transform.position);

        //check if transform position is 
        if (dist <= triggerDistance)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    private void ShowIcon(float time)
    {
        LeanTween.alpha(FloatingIcon, 0.6f, time).setEaseOutCubic();
        _IsShown = true;
    }


    private void HideIcon(float time)
    {
        if (gameObject.GetComponent<MeshRenderer>().material.color.a > 0)
        LeanTween.alpha(FloatingIcon, 0.0f, time).setEaseOutCubic();
        _IsShown = false;
    }

    public void LookAtPlayer(GameObject source, GameObject target)
    {
        if(_IsShown)
        {
            if(source != null)
            {
                if (target != null)
                {
                    source.transform.LookAt(2 * source.transform.position - target.transform.position);
                }
            }

            
        }/* else if (gameObject.GetComponent<MeshRenderer>().material.color.a > 0);
            HideIcon(0.1f);*/
    }


    //guckt der player das icon an?
    /*
    public bool IsFacing(GameObject other, float angle)
    {
        if (Vector3.Angle(other.transform.forward, transform.position - other.transform.position) < angle)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    */

}
