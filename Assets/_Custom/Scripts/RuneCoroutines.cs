﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class RuneCoroutines : MonoBehaviour
{
    [SerializeField]
    private GameObject Player = null;

    public ParticleSystem fxProximity = null;
    public ParticleSystem fxSocketed = null;
    public ParticleSystem fxActivate = null;
    public ParticleSystem fxHologram = null;

    public AudioSource SoundSelect = null;
    public AudioSource SoundActivate = null;
    public AudioSource SoundHologramLerp = null;
    public AudioSource SoundGoldLerp = null;

    public Transform AttachHologram = null;

    [SerializeField]
    private BoxCollider Col = null;

    //private float time = 6f;

    public float ProximityDistance = 3.0f;

    [SerializeField]
    private XRSocketInteractor SocketInteractor;

    public GameObject selectedInteractable;


    void Awake()
    {
        AttachHologram.transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
        AttachHologram.gameObject.SetActive(false);

        //SocketInteractor = gameObject.GetComponent<XRSocketInteractor>();
    }

    private void Start()
    {
        StartCoroutine(CoroutineProximityChecker());
    }

    public void SocketRune()
    {
        //PlayFX
        StartCoroutine(PlayFX(fxActivate));
        StartCoroutine(StopFX(fxProximity));

        SoundSelect.Play();
        SoundActivate.PlayDelayed(1.0f);

        if(SocketInteractor.selectTarget.gameObject.GetComponent<BoxCollider>() != null)
            SocketInteractor.selectTarget.gameObject.GetComponent<BoxCollider>().enabled = false;
    }

    private bool CheckProximity(Transform Source, Transform Target)
    {
        float dist = Vector3.Distance(Source.position, Target.position);


        if (dist <= ProximityDistance)
            return true;
        else
            return false;
    }

    public void StartHologramLerp()
    {
        DeactivateCollider();
        StartCoroutine(LerpHologram(AttachHologram.gameObject, new Vector3(1f, 1f, 1f), 3f));
    }

    public void StartGoldLerp(Transform pos)
    {
        StartCoroutine(LerpGold(pos));

        SoundGoldLerp.Play();
    }

    private void DeactivateCollider()
    {
        if(Col != null)
            Col.enabled = false;
    }

    IEnumerator CoroutineProximityChecker()
    {
        while (SocketInteractor.selectTarget == null)
        {
            if (CheckProximity(gameObject.transform, Player.transform))
            {
                StartCoroutine(PlayFX(fxProximity));
            }
            else
                StartCoroutine(StopFX(fxProximity));

            yield return new WaitForSeconds(0.5f);
        }

        StartCoroutine(StopFX(fxProximity));
        yield return null;
    }

    IEnumerator LerpHologram(GameObject Object, Vector3 target, float time)
    {
        SoundHologramLerp.Play();

        float speed = 0.05f;
        float i = 0.0f;
        float rate = (1.0f / time) * speed;

        while (i < 1.0f)
        {
            i += Time.deltaTime * rate;

            Object.transform.localScale = Vector3.Lerp(Object.transform.localScale, target, i);
            yield return null;
        }

        yield return null;
    }

    IEnumerator LerpGold(Transform pos)
    {
        //float time2 = 100f;

        XRSocketInteractor RuneSocket = GetComponent<XRSocketInteractor>();

        if (RuneSocket.selectTarget != null)
        {
            GameObject Gold = RuneSocket.selectTarget.gameObject;

            float time = 2.55f;

            LeanTween.move(Gold,pos.transform.position, time);

            yield return new WaitForSeconds(time);


            Gold.SetActive(false);

            yield return null;
        }
    }

    IEnumerator PlayFX(ParticleSystem ParticleFX)
    {
        // Check if FX is already Playing
        while (ParticleFX.IsAlive())
            yield return new WaitForSeconds(0.2f);
        
        ParticleFX.Play();

        yield return null;
    }

    IEnumerator StopFX(ParticleSystem ParticleFX)
    {
        if (ParticleFX.IsAlive())
            ParticleFX.Stop();

        yield return null;
    }
}
