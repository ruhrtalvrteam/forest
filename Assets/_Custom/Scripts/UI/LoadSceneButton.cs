﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LoadSceneButton : MonoBehaviour
{
    [SerializeField]
    private int sceneToLoad = 0;

    [SerializeField]
    private Button StartButton = null;

    [SerializeField]
    private Canvas HomeScreen = null;

    [SerializeField]
    private Canvas LoadingScreen = null;

    [SerializeField]
    private ProgressSceneLoader SceneLoader = null;

    // Start is called before the first frame update
    void Start()
    {
        StartButton.onClick.AddListener(() =>
        {
            LoadingScreen.gameObject.SetActive(true);

            HomeScreen.gameObject.SetActive(false);
            SceneLoader.LoadScene(sceneToLoad);
        });
    }
}
