﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCharacter : MonoBehaviour
{
    public GameObject character;

    // Start is called before the first frame update
    public void TriggerSpawn()
    {
        if(character != null)
        {
            character.SetActive(true);
        }
    }
}
