﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shrink : MonoBehaviour
{
    private float wait = 2.0f;

    // Update is called once per frame
    void Update()
    {
        if (wait >= 0)
        {
            wait -= Time.deltaTime;
        }
        else
        {
            Rigidbody rig = transform.GetComponent<Rigidbody>();

            rig.isKinematic = true;
            rig.useGravity = false;
            transform.localScale = Vector3.Lerp(transform.localScale, transform.localScale * 0, Time.deltaTime * 2);
            
        }
    }
}
