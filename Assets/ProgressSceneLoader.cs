﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ProgressSceneLoader : MonoBehaviour
{
    [SerializeField]
    private Canvas LoadingScreen = null;

    [SerializeField]
    private Image progressImage = null;

    [SerializeField]
    private Text progressText = null;

    private AsyncOperation operation;
    //private Canvas canvas;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
    
    public void LoadScene(int sceneInt)
    {
        UpdateProgressUI(0);
        LoadingScreen.gameObject.SetActive(true);

        StartCoroutine(BeginLoad(sceneInt));
    }

    private IEnumerator BeginLoad(int sceneInt)
    {
        operation = SceneManager.LoadSceneAsync(sceneInt);

        while (!operation.isDone)
        {
            UpdateProgressUI(operation.progress);
            yield return null;
        }

        UpdateProgressUI(operation.progress);
        operation = null;
        LoadingScreen.gameObject.SetActive(false);
    }

    private void UpdateProgressUI(float progress)
    {
        progressImage.fillAmount = progress;
        progressText.text = (int)(progress * 100f) + " %";
    }
}
