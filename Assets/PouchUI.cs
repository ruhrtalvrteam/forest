﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit;

public class PouchUI : MonoBehaviour
{
    [SerializeField]
    XRSocketInteractor[] Pouches = null;

    [SerializeField]
    Text text = null;

    private void FixedUpdate()
    {
        if(Pouches.Length > 0)
        {
            int i = 0;

            foreach (XRSocketInteractor Pouch in Pouches)
            {
                if (Pouch.selectTarget != null)
                    i++;
            }

            if (text != null)
                text.text = i + "/" + Pouches.Length;
        }
    }
}
