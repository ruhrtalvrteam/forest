﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemFollow : MonoBehaviour
{
    public Transform target;

    void FixedUpdate()
    {
        transform.position = target.position;
        transform.rotation = target.rotation;
    }
}
