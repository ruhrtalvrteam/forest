﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SkinnedMeshRenderer))]
public class DissolveOverTime : MonoBehaviour
{
    private SkinnedMeshRenderer meshRenderer;

    private void Awake(){
        meshRenderer = this.GetComponent<SkinnedMeshRenderer>();
    }

    [SerializeField]
    private float speed = 1.0f;

    [SerializeField]
    private float value;

    void Update()
    {
        if (value < 10)
        {
            //cutoffpercent = Mathf.Sin(t * speed);

            Material[] mats = meshRenderer.materials;

            mats[0].SetFloat("_CutoffHeight", value);
            //t += Time.deltaTime;

            // Unity does not allow meshRenderer.materials[0]...
            meshRenderer.materials = mats;

            value += 0.1f * speed;
        }
    }
}
