﻿using System.Collections;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class Arrow : XRGrabInteractable
{
    public float speed = 2000.0f;
    public Transform tip = null;

    private bool inAir = false;
    private Vector3 lastPosition = Vector3.zero;

    private Rigidbody rigidBody = null;

    public GameObject TrailFX = null;

    protected override void Awake()
    {
        base.Awake();
        rigidBody = GetComponent<Rigidbody>();

        // TrailFX behind Arrow
        if (TrailFX != null)
        {
            TrailFX.SetActive(false);
        }
    }

    private void FixedUpdate()
    {
        if(inAir)
        {
            CheckForCollision();
            lastPosition = tip.position;
        }
    }

    private void CheckForCollision()
    {
        var layerMask = (1 << 8) | (1 << 9) | (1 << 13);
        layerMask = ~layerMask;

        if (Physics.Linecast(lastPosition, tip.position, out RaycastHit hit, layerMask))
        {
            Stop(hit.collider.gameObject);
        }
    }

    private void Stop(GameObject hitObject)
    {
        // Flag
        inAir = false;

        // Parent
        transform.parent = hitObject.transform;

        // Disable physics
        SetPhysics(false);

        // Damage
        CheckForDamage(hitObject);

        // TrailFX behind Arrow
        if (TrailFX != null)
        {
            TrailFX.SetActive(false);
        }
    }

    private void CheckForDamage(GameObject hitObject)
    {
        //Use an Interface (IDamageable) to push damage through to the hit Object

        MonoBehaviour[] behaviours = hitObject.GetComponents<MonoBehaviour>();

        foreach (MonoBehaviour behaviour in behaviours)
        {
            if (behaviour is IDamageable)
            {
                IDamageable damageable = (IDamageable)behaviour;
                damageable.Damage(5);

                break;
            }
        }
    }

    public void Release(float pullValue)
    {
        inAir = true;
        SetPhysics(true);

        MaskAndFire(pullValue);
        StartCoroutine(RotateWithVelocity());

        lastPosition = tip.position;

        // TrailFX behind Arrow
        if (TrailFX != null)
        {
            TrailFX.SetActive(true);
        }
    }

    private void SetPhysics(bool usePhysics)
    {
        rigidBody.isKinematic = !usePhysics;
        rigidBody.useGravity = usePhysics;
    }

    private void MaskAndFire(float power)
    {
        colliders[0].enabled = false;
        interactionLayerMask = 1 << LayerMask.NameToLayer("Ignore");

        Vector3 force = transform.forward * (power * speed);
        rigidBody.AddForce(force);
    }

    private IEnumerator RotateWithVelocity()
    {
        yield return new WaitForFixedUpdate();

        while(inAir)
        {
            Quaternion newRotation = Quaternion.LookRotation(rigidBody.velocity, transform.up);
            transform.rotation = newRotation;
            yield return null;
        }
    }

    public new void OnSelectEnter(XRBaseInteractor interactor)
    {
        base.OnSelectEnter(interactor);
    }

    public new void OnSelectExit(XRBaseInteractor interactor)
    {
        base.OnSelectExit(interactor);
    }
}
